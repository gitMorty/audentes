# README #

Audentes is an android application for in-classroom communication   

### What is this repository for? ###

This repository contains all the source code for the app


### How do I get set up? ###

Get our app, by downloading the APK file from this link :  [Audentes](https://github.com/gtr1234/AnonymousDoubts/releases/)

[Project Website](https://sites.google.com/site/audentes456/)
